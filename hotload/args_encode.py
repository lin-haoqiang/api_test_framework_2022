import base64
import hashlib
import random
import time
import rsa
import yaml

from hotload.base_hotload import BaseHotload
from util.requests_util import RequestsUtil
from util.yaml_util import get_project_path


class ArgsEncode(BaseHotload):

    # 获得随机时间
    @staticmethod
    def get_random_time():
        return str(int(time.time()))[1:6]

    # MD5加密
    @staticmethod
    def md5(args):
        # 以指定的编码格式编码字符串
        utf8_str = str(args).encode("utf-8")
        # md5加密（哈希算法）
        """
        hashlib.md5() : 获取一个md5加密算法对象
        """
        md5_str = hashlib.md5(utf8_str).hexdigest()  # 获取加密后的16进制字符串
        return md5_str.upper()

    # BASE64加密
    @staticmethod
    def bs64(args):
        # 以指定的编码格式编码字符串，返回一个字节对象（bytes）。
        utf8_str = str(args).encode("utf-8")
        # base64加密
        """
        base64.b64encode(): 为一个bytes-like对象进行编码，并返回一个字节对象。
        """
        base64_str = base64.b64encode(utf8_str).decode("utf-8")
        return base64_str.upper()

    # RSA双钥加密方式
    # 生成公钥和私钥写入到指定的pem文件
    @staticmethod
    def create_key():
        # Generates public and private keys, and returns them as (pub, priv)
        # pub和priv是对象
        (public_key, private_key) = rsa.newkeys(1024)
        # 保存公钥
        with open("public.pem", "w+") as f:
            f.write(public_key.save_pkcs1().decode())  # save_pkcs1()返回字节对象
        # 保存私钥
        with open("private.pem", "w+") as f:
            f.write(private_key.save_pkcs1().decode())

    # 通过公钥加密
    @staticmethod
    def public_key_encode(args):
        # 导入公钥
        with open("public.pem") as f:
            pubkey = rsa.PublicKey.load_pkcs1(f.read().encode())
        # 加密
        byte_str = rsa.encrypt(str(args).encode("utf-8"), pubkey)
        # print(byte_str)
        # 把二进制转化成字符串格式
        miwen = base64.b64encode(byte_str).decode("utf-8")
        return miwen

    # 通过私钥解密
    @staticmethod
    def private_key_decode(args):
        # 导入私钥
        with open("private.pem") as f:
            prikey = rsa.PrivateKey.load_pkcs1(f.read().encode())
        # 把字符串转换成二进制格式
        byte_str = base64.b64decode(args)
        # 解密
        mingwen = rsa.decrypt(byte_str, prikey).decode()
        return mingwen

    # Sign签名（签名规则请看README.txt）
    def signs(self, yaml_path):
        last_url = ""
        last_data = {}
        with open(get_project_path() + yaml_path, encoding="utf-8") as f:
            yaml_value = yaml.load(f, Loader=yaml.FullLoader)
            for caseinfo in yaml_value:
                caseinfo_keys = caseinfo.keys()
                # 判断一级关键字是否包括有:name,request,valiedate
                if "request" in caseinfo_keys:
                    # 判断url
                    if "url" in caseinfo['request'].keys():
                        last_url = caseinfo['request']['url']
                    # 判断参数
                    req = caseinfo['request']
                    for key, value in req.items():
                        if key in ['params', 'data', 'json']:
                            for p_key, p_value in req[key].items():
                                last_data[p_key] = p_value
        # 获取url中的请求参数字符串，即?后面的内容
        last_url = last_url[last_url.index("?") + 1:len(last_url)]

        # 把last_url的字符串格式加到last_data字典
        lis = last_url.split("&")
        for a in lis:
            last_data[a[0:a.index("=")]] = a[a.index("=") + 1:len(a)]

        # 热加载替换值
        """
        之所以要在这里执行RequestsUtil(self).replace_value(last_data)，
        是因为当执行该方法signs()时，字段'params', 'data', 'json'还没在request_util.py的replace_value()方法所进行模板字符串的替换。
        request_util.py的replace_value()是循环字符串${}，所以没办法控制signs()方法在循环的最后一次执行。
        所以我们要在这里为该yaml文件剩下的模板${}给替换掉，不然我们就只会对yaml文件的模板进行签名了。
        """
        last_data = RequestsUtil(self).replace_value(last_data)

        # 字典根据key的ascii码排序
        new_dict = self.dict_asscii_sort(last_data)

        # 第二步：(2)把参数名和参数的值用=连接成字符串，多个参数之间用&连接。a=2&b=1&c=3
        new_str = ""
        for key, value in new_dict.items():
            new_str = new_str + key + "=" + value + "&"
        # 第三到第五步
        appid = "appid=" + "wx8a9de038e93f77ab" + "&"
        appsecret = "appsecret=" + "8326fc915928dee3165720c910effb86" + "&"
        nonce = "nonce=" + str(random.randint(1000000000, 9999999999)) + "&"
        timestamp = "timestamp=" + str(time.time())
        all_str = appid + appsecret + new_str + nonce + timestamp
        # 第六步
        sign_str = self.md5(all_str)
        return sign_str

    # 字典的key根据asscii排序
    def dict_asscii_sort(self, dict_str):
        dict_key = dict(dict_str).keys()
        l = list(dict_key)
        l.sort()
        new_dict = {}
        for key in l:
            new_dict[key] = dict_str[key]
        return new_dict


if __name__ == "__main__":
    # ArgsEncode().create_key()
    before_encode = ArgsEncode().public_key_encode("dwyane")
    after_decode = ArgsEncode().private_key_decode(before_encode)
    print(after_decode)
