import random

from hotload.base_hotload import BaseHotload


class DebugTalk(BaseHotload):

    # 获得随机数
    @staticmethod
    def get_random_number(min, max):
        return str(random.randint(int(min), int(max)))
