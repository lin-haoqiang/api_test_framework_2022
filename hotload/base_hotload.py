from util.yaml_util import read_extract_yaml, read_config_yaml, get_project_path


class BaseHotload:
    # 读取extract.yaml文件中的值
    @staticmethod
    def read_extract_data(key):
        return read_extract_yaml(key)

    # 读取config.yaml
    @staticmethod
    def read_config(one_node, two_node):
        return read_config_yaml(one_node, two_node)
