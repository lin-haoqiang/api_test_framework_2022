import allure
import pytest

from hotload.debug_talk import DebugTalk
from util.parametrize_util import read_testcase_yaml
from util.requests_util import RequestsUtil


@allure.epic("Dwyane接口自动化测试框架")
@allure.feature("微信接口测试模块")
@allure.story("微信标签测试模块")
class TestWxTagApi:
    ru = RequestsUtil(DebugTalk())

    # @pytest.mark.run(order=1)
    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/wx_api/yml/wx_get_token.yml"))
    def test_get_token(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestWxTagApi.ru.standard_yaml(case_info)

    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/wx_api/yml/wx_create_flag.yml"))
    def test_create_flag(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestWxTagApi.ru.standard_yaml(case_info)

    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/wx_api/yml/wx_delete_flag.yml"))
    def test_delete_flag(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestWxTagApi.ru.standard_yaml(case_info)

    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/wx_api/yml/wx_get_flag.yml"))
    def test_get_flag(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestWxTagApi.ru.standard_yaml(case_info)

    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/wx_api/yml/wx_file_upload.yml"))
    def test_file_upload(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestWxTagApi.ru.standard_yaml(case_info)
