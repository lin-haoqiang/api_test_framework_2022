import allure
import pytest

from hotload.args_encode import ArgsEncode
from util.parametrize_util import read_testcase_yaml
from util.requests_util import RequestsUtil


@allure.epic("Dwyane接口自动化测试框架")
@allure.feature("加密测试模块")
class TestEncodePracticeApi:
    ru = RequestsUtil(ArgsEncode())

    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/encode_practice/yml/ep_md5.yml"))
    def test_md5(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestEncodePracticeApi.ru.standard_yaml(case_info)

    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/encode_practice/yml/ep_base64.yml"))
    def test_base64(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestEncodePracticeApi.ru.standard_yaml(case_info)

    @pytest.mark.parametrize("case_info", read_testcase_yaml("/testcase/encode_practice/yml/ep_sign.yml"))
    def test_sign(self, case_info):
        allure.dynamic.title(case_info["name"])
        TestEncodePracticeApi.ru.standard_yaml(case_info)
