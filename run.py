import os
import time

import pytest

# 删除logs日志文件
from util.yaml_util import get_project_path


def delete_logs():
    logs_path = get_project_path() + "/log/"
    file_list = os.listdir(logs_path)
    # 之所以不在夹具中执行是因为，在pytest启动后就开始在写入日志文件，
    # 因为此时os.listdir(logs_path)已经能读取到本次启动时正在打开和写入的log文件，而这个文件我们是不能删除的
    for f in file_list:
        os.remove(logs_path + f)


if __name__ == '__main__':
    delete_logs()
    pytest.main()
    time.sleep(1)
    os.system("allure generate ./temp -o ./report --clean")
