import json
import traceback

import yaml

from util.logger_util import log_error

# 读取testcase目录下的yaml.（加载后的该yaml文件格式为：[{}] ）
from util.yaml_util import get_project_path, read_data_yaml


def read_testcase_yaml(yaml_path):
    try:
        with open(get_project_path() + yaml_path, encoding="utf-8") as f:
            case_info_list = yaml.load(f, Loader=yaml.FullLoader)
            # 大于两个元素时，则直接充当ddt
            if len(case_info_list) >= 2:
                return case_info_list
            else:  # 只有一个元素时
                # *caseinfo是指将[{}]的数组解构。
                if "parameterize" in dict(*case_info_list).keys():
                    return ddt(*case_info_list)
                else:
                    return case_info_list
    except Exception as e:
        log_error("读取测试用例方法read_testcase_yaml异常：%s" % str(traceback.format_exc()))


def ddt(case_info):
    # 进来的是一个对象。testcase目录下的yaml的[{}]里面的{}。
    try:
        if "parameterize" in case_info.keys():
            # 把字典转换为json字符串
            caseinfo_str = json.dumps(case_info)
            # 处理testcase目录下的yaml文件的parameterize字段的值
            for param_key, param_value in case_info["parameterize"].items():
                key_list = param_key.split("-")

                """-----规范yaml数据文件的写法-------"""
                # 规范yaml数据文件的写法
                length_flag = True
                # 读取data目录下的yaml.(加载后的该yaml文件是一个数组，一个case一个元素。但第一个元素是字段名)
                # 格式如[[字段名1,字段名2,..], [数据1, 数据2, ..], [数据1, 数据2, ..]]
                data_list = read_data_yaml(param_value)  # 这个param_value其实是data目录下的数据驱动的yaml文件的路径

                for data in data_list:
                    if len(data) != len(key_list):
                        length_flag = False
                        break

                """-----替换值:把模板字符串$ddt{}替换掉-------"""
                new_caseinfo_list = []
                if length_flag:
                    for x in range(1, len(data_list)):  # 循环数据行数。（不是从0开始，是因为第一行是字段名）
                        temp_caseinfo = caseinfo_str
                        for y in range(0, len(data_list[x])):  # 循环数据列
                            # 下面要做的事就是要更新testcase目录下的yml文件，以前只有[{}]，现在要更新为[{},{},{},...]
                            if data_list[0][y] in key_list:  # data_list[0]就是指data目录下yml文件的第一行的字段名数组
                                # 替换原始的yaml里面的$ddt{}
                                temp_caseinfo = temp_caseinfo.replace("$ddt{" + data_list[0][y] + "}",
                                                                      str(data_list[x][y]))
                            else:
                                raise Exception("ddt的yaml文件的字段名不匹配")
                        new_caseinfo_list.append(json.loads(temp_caseinfo))
                else:
                    raise Exception("ddt的yaml文件的数组长度不匹配")
                return new_caseinfo_list
        else:
            return case_info
    except Exception as e:
        log_error("数据驱动方法ddt异常：%s" % str(traceback.format_exc()))
