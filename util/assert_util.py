import json
import traceback
import jsonpath
from util.logger_util import log_error, LoggerUtil

log = LoggerUtil()


# 断言判断
def assert_result(expected_result, actual_result, return_code):
    """

    :param expected_result: 来自yaml文件的validate字段，是一个数组。
                            数组的每个元素是一个字典，该字典通常只有一个键，该键的值也可以是一个字典。
                            所以该数组通常是：[{'equals': {'status_code': 200, 'message': 'success'}}, {'contains': tag}]
    :param actual_result: 实际响应文本（json字符串）
    :param return_code: http响应的状态码
    :return:
    """
    try:
        log.info("预期结果：%s" % expected_result)
        log.info("实际结果：%s" % json.loads(json.dumps(actual_result).replace(r"\\", "\\")))  # 把字符串的\\替换为一个反斜杠\
        # 进行replace是为了解决响应文本若出现unicode编码如\\u6d69，多出现了一条斜杠的情况。（正常只有一条反斜杠）

        all_flag = 0
        for yq in expected_result:
            for key, value in yq.items():
                if key == "equals":
                    flag = equals_assert(value, return_code, actual_result)
                    all_flag = all_flag + flag
                elif key == "contains":
                    flag = contains_assert(value, actual_result)
                    all_flag = all_flag + flag
                else:
                    log_error("框架暂不支持此断言方式")
        assert all_flag == 0
        log.info("接口测试成功")
        log.info("----------接口测试结束----------\n")
    except Exception as e:
        log.info("接口测试失败!!!")
        log.info("----------接口测试结束----------\n")
        log_error("断言assert_result异常：%s" % str(traceback.format_exc()))


# 相等断言
def equals_assert(value, return_code, actual_result):
    flag = 0
    for assert_key, assert_value in value.items():
        if assert_key == "status_code":  # 状态断言
            if assert_value != return_code:
                flag = flag + 1
                log_error("断言失败：返回的状态码不等于%s" % assert_value)
        else:
            # 判断该请求的响应json文本是否存在字段assert_key
            lists = jsonpath.jsonpath(actual_result, '$..%s' % assert_key)
            if lists:
                if assert_value not in lists:
                    flag = flag + 1
                    log_error("断言失败：" + assert_key + "不等于" + str(assert_value))
            else:
                flag = flag + 1
                log_error("断言失败：返回的结果中不存在：" + assert_key)
    return flag


# 包含断言
def contains_assert(value, actual_result):
    flag = 0
    if str(value) not in str(actual_result):
        flag = flag + 1
        log_error("断言失败：返回的结果中不包含：" + str(value))
    return flag
