import json
import re
import traceback

import jsonpath
import requests

from util.assert_util import assert_result
from util.logger_util import log_error, LoggerUtil
from util.yaml_util import write_extract_yaml

log = LoggerUtil()


class RequestsUtil:

    def __init__(self, hotload_obj):
        # requests.session()能够保持各个http请求的cookie，也能在该session中设置自定义header，使得该session下的请求都发送同样的header
        # 因为http请求之间是无状态的。
        self.s = requests.session()
        self.hotload_obj = hotload_obj

    def request(self, name, method, url, **kwargs):
        try:
            # 请求方法处理
            method = str(method).lower()
            # 基础路径的拼接以及替换
            url = self.replace_value(url)
            # 请求参数、请求体和请求头的反射字符串模板的替换
            for key, value in kwargs.items():
                if key in ["params", "data", "json", "headers"]:
                    kwargs[key] = self.replace_value(value)
                elif key == "files":
                    for file_key, file_path in value.items():
                        value[file_key] = open(file_path, "rb")
                        # 注意这里不能用with as，用了with as就直接关闭文件流了，正式发送请求就会读取不到
            # 输入信息日志
            log.info("接口名称：%s" % name)
            log.info("请求方式：%s" % method)
            log.info("请求路径：%s" % url)
            if "headers" in kwargs.keys():
                log.info("请求头：%s" % kwargs["headers"])
            if "params" in kwargs.keys():
                log.info("请求params参数：%s" % kwargs["params"])
            elif "data" in kwargs.keys():
                log.info("请求data参数：%s" % kwargs["data"])
            elif "json" in kwargs.keys():
                log.info("请求json参数：%s" % kwargs["json"])
            if "files" in kwargs.keys():
                log.info("文件上传：%s" % kwargs["files"])

            # 请求
            res = self.s.request(method, url, **kwargs)
            return res
        except Exception as e:
            log_error("发送请求send_request异常：%s" % str(traceback.format_exc()))

    # 规范YAML测试用例
    def standard_yaml(self, case_info):
        try:
            log.info("----------接口测试开始----------")

            case_info_keys = case_info.keys()
            # 判断一级关键字是否包括有:name,request,validate
            if "name" in case_info_keys and "base_url" in case_info_keys and "request" in case_info_keys and "validate" in case_info_keys:
                # 判断request下面是否包含：method,url
                request_keys = case_info["request"].keys()
                if "method" in request_keys and "url" in request_keys:
                    # 发送请求
                    name = case_info.pop("name")  # 删除字典给定键 key 所对应的值，返回值为被删除的值
                    base_url = case_info.pop("base_url")
                    method = case_info["request"].pop("method")
                    url = case_info["request"].pop("url")
                    url = base_url + url
                    res = self.request(name, method, url, **case_info["request"])
                    return_text = res.text
                    return_code = res.status_code
                    log.info(f"响应文本：{return_text}")
                    try:
                        return_json = res.json()  # 返回一个对象，非字符串
                    except Exception as e:
                        log_error("返回的结果不是JSON格式")
                    # 提取关联的值并且写入extract.yaml文件
                    if "extract" in case_info_keys:
                        for key, value in case_info["extract"].items():
                            # 根据实际情况选择用正则方式提取还是用jsonpath来提取
                            if "(.*?)" in value or "(.+?)" in value:  # 正则
                                zz_value = re.search(value, return_text)
                                if zz_value:
                                    extract_value = {key: zz_value.group(1)}
                                    write_extract_yaml(extract_value)
                            else:  # jsonpath
                                js_value = jsonpath.jsonpath(return_json, value)
                                # 这里无论用$..还是$.都是会返回数组
                                if js_value:
                                    extract_value = {key: js_value[0]}
                                    write_extract_yaml(extract_value)
                    # 断言
                    expected_result = case_info["validate"]
                    actual_result = return_json
                    assert_result(expected_result, actual_result, return_code)
                else:
                    log_error("在request下必须包含：method,url")
            else:
                log_error("一级关键字必须要包含：name,base_url,request,validate")
        except Exception as e:
            log_error("规范YAML测试用例standard_yaml异常：%s" % str(traceback.format_exc()))

    # 替换值的方法
    # 考虑问题1：(替换url,params,data,json,headers)
    # 考虑问题2：(string,int,float,list,dict)
    def replace_value(self, data):
        if data:
            # 保存数据类型
            data_type = type(data)
            # 判断数据类型，把所有数据转为为字符串，方便后面来识别'${'字符串模板来进行方法的反射
            if isinstance(data, dict) or isinstance(data, list):
                str_data = json.dumps(data)
            else:
                str_data = str(data)
            # 替换
            for cs in range(1, str_data.count('${') + 1):
                if "${" in str_data and "}" in str_data:
                    start_index = str_data.index("${")
                    end_index = str_data.index("}", start_index)
                    old_value = str_data[start_index:end_index + 1]  # old_value：${read_yaml()}
                    # 反射：通过类的对象和方法字符串调用方法
                    func_name = old_value[2:old_value.index('(')]
                    # args_value1：${read_yaml()}里括号里的实参字符串
                    args_value1 = old_value[old_value.index('(') + 1:old_value.index(')')]
                    new_value = ""
                    if args_value1 != "":
                        args_value2 = args_value1.split(',')  # 数组
                        # 利用反射正式调用方法。（getattr的第一个参数：func_name所在的对象
                        new_value = getattr(self.hotload_obj, func_name)(*args_value2)
                    else:
                        new_value = getattr(self.hotload_obj, func_name)()

                    str_data = str_data.replace(old_value, str(new_value))
            # 还原数据类型
            if isinstance(data, dict) or isinstance(data, list):
                data = json.loads(str_data)
            else:
                data = data_type(str_data)
        return data
