import logging
import time
from util.yaml_util import get_project_path, read_config_yaml


class LoggerUtil:
    logger = None

    def __new__(cls, *args, **kwargs):
        if cls.logger is None:
            cls.logger = cls.get_logger(*args)
        return cls.logger

    @classmethod
    def get_logger(cls, logger_name="Dwyane"):
        # 创建一个日志对象
        logger = logging.getLogger(logger_name)
        # 设置全局的日志级别（从低到高：debug调试<info信息<warning警告<error错误<critical严重）
        logger.setLevel(logging.DEBUG)

        # 去除重复的日志.(若logger的handlers为空，即没有配置handler，才进入if语句)

        # 注意，pytest启动时，是会自动配置rootlogger.handlers的，所以此时logger.handlers是有值的。
        # 所以我们要么不用rootLogger，即要在上方getLogger(logger_name)。
        # 要么在使用rootLogger的时候，在下方判断，不然不会进入if not logger.handlers的条件。

        if not logger.handlers:
            # ----------文件日志----------
            # 1.创建文件日志路径
            file_log_path = get_project_path() + "/log/" + \
                            read_config_yaml("log", "log_filename_prefix") + \
                            time.strftime('%y%m%d_%H%M%S', time.localtime()) + ".log"
        # 2.创建文件日志的控制器
        file_handler = logging.FileHandler(file_log_path, encoding="utf-8")
        # 3.设置文件日志的日志级别
        file_log_level = str(read_config_yaml("log", "log_level")).lower()
        if file_log_level == "debug":
            file_handler.setLevel(logging.DEBUG)
        elif file_log_level == "info":
            file_handler.setLevel(logging.INFO)
        elif file_log_level == "warning":
            file_handler.setLevel(logging.WARNING)
        elif file_log_level == "error":
            file_handler.setLevel(logging.ERROR)
        elif file_log_level == "critical":
            file_handler.setLevel(logging.CRITICAL)
        else:
            file_handler.setLevel(logging.DEBUG)
        # 4.创建文件日志的格式
        file_handler.setFormatter(logging.Formatter(read_config_yaml("log", "log_format")))
        # 将文件日志的控制器加入到日志对象
        logger.addHandler(file_handler)

        # ----------控制台日志----------
        # 1.创建控制台日志的控制器
        console_handler = logging.StreamHandler()
        # 2.设置控制台日志的日志级别
        console_log_level = str(read_config_yaml("log", "log_level")).lower()
        if console_log_level == "debug":
            console_handler.setLevel(logging.DEBUG)
        elif console_log_level == "info":
            console_handler.setLevel(logging.INFO)
        elif console_log_level == "warning":
            console_handler.setLevel(logging.WARNING)
        elif console_log_level == "error":
            console_handler.setLevel(logging.ERROR)
        elif console_log_level == "critical":
            console_handler.setLevel(logging.CRITICAL)
        else:
            console_handler.setLevel(logging.DEBUG)
        # 3.创建控制台日志的格式
        console_handler.setFormatter(logging.Formatter(read_config_yaml("log", "log_format")))
        # 将控制台日志的控制器加入到日志对象
        logger.addHandler(console_handler)

        # 返回包含有文件日志控制器和控制台日志控制器的日志对象
        return logger


# 错误日志的输出
def log_error(message):
    LoggerUtil().error(message)
    raise Exception(message)


# 信息日志的输出
def log_info(message):
    LoggerUtil().info(message)


if __name__ == '__main__':
    log_info("这是一个info")
    print(LoggerUtil() == LoggerUtil())
