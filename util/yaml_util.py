import os
import yaml


# 获取项目根目录
def get_project_path():
    project_name = "api_test_framework_2022"
    # os.path.abspath还有另外一个作用是：去掉最后多余的斜杠
    prev_project_path = os.path.abspath(__file__).split(project_name)[0]
    return os.path.join(prev_project_path, project_name)


# 读取extract.yaml
def read_extract_yaml(key):
    with open(get_project_path() + "/extract.yml", encoding="utf-8") as f:
        value = yaml.load(f, Loader=yaml.FullLoader)
        return value[key]


# 写入extract.yaml
def write_extract_yaml(data):
    with open(get_project_path() + "/extract.yml", encoding="utf-8", mode="a") as f:
        yaml.dump(data=data, stream=f, allow_unicode=True)


# 清空extract.yaml
def clear_extract_yaml():
    with open(get_project_path() + "/extract.yml", encoding="utf-8", mode="w") as f:
        f.truncate()


# 读取config.yaml
def read_config_yaml(one_node, two_node):
    with open(get_project_path() + "/config_test_env.yml", encoding="utf-8") as f:
        value = yaml.load(f, Loader=yaml.FullLoader)
        return value[one_node][two_node]


# 读取数据的yaml
def read_data_yaml(yaml_path):
    with open(get_project_path() + yaml_path, encoding="utf-8") as f:
        value = yaml.load(f, Loader=yaml.FullLoader)
        # yaml.FullLoader：加载完整的YAML语言。避免任意代码执行。这是当前（PyYAML5.1）默认加载器调用
        # yaml.load(input)（发出警告后）。
        return value
