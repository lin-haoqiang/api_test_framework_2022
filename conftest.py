import os

import pytest

from util.yaml_util import clear_extract_yaml


@pytest.fixture(scope="session", autouse=True)
def clear_extract():
    clear_extract_yaml()
